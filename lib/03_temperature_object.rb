class Temperature
  def initialize(choice)
    if choice.key?(:f)
      @temp, @scale = choice[:f], "fahrenheit"
    else
      @temp, @scale = choice[:c], "celsius"
    end
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def in_celsius
    @scale == "celsius" ? @temp : (@temp - 32) * 5 / 9
  end

  def in_fahrenheit
    @scale == "fahrenheit" ? @temp : @temp * 9.0 / 5.0 + 32
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp, @scale = temp, "celsius"
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp, @scale = temp, "fahrenheit"
  end
end
