class Book
  attr_accessor :title


  def title=(title)
    capitalized_arr = []
    lower_words = ['and', 'in', 'of', 'the', 'a', 'an']
    title.split.map.each_with_index do |word, i|
      if i == 0 || !lower_words.include?(word)
        capitalized_arr << word.capitalize
      else
        capitalized_arr << word
      end
    end
    @title = capitalized_arr.join(" ")
  end
end
