class Dictionary

  def initialize
    @d = {}
  end


  def entries
    @d
  end

  def keywords
    @d.keys.sort
  end

  def find(word)
    return_hash = {}
    entries.each do |k, v|
      length = word.length
      return_hash[k] = v if word == k[0...length] && k != nil
    end
    return_hash
  end

  def include?(word)
    keywords.include?(word)
  end

  def add(input)
    if input.class.to_s == 'Hash'
      converted = input
    else
      converted = { input => nil }
    end
    converted.each { |k, v| @d[k] = v }
  end

  def printable
    entries = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@d[keyword]}"}
    end
    entries.join("\n")
  end
end
