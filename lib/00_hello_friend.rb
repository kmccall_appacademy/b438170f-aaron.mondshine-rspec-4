class Friend
  def greeting(person = nil)
    person = ", #{person}" unless person == nil
    "Hello#{person}!"
  end
end
