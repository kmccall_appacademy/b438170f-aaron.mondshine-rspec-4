class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds % 3600) / 60
    seconds = @seconds % 60
    "#{padder(hours)}:#{padder(minutes)}:#{padder(seconds)}"

  end

  def padder(n)
    n < 10 ? "0#{n}" : n
  end
end
